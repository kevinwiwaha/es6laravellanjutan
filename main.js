// Soal 1
const golden = () => {
    console.log("This is golden!");
}
golden();

// Soal 2
const literal = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullname: function () {
            return console.log(firstName + lastName);
        }

    }
}

//Driver Code 
literal("William", "Imoh").fullname();

// Soal 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;
console.log(firstName, lastName);

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [west, east]
//Driver Code
console.log(combined)


// SOal 6
const planet = "earth"
const view = "glass"
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet} do eiusmod tempot incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before) 